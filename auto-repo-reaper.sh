#!/bin/bash

# REFERENCES:
# https://hub.github.com
# https://developer.github.com/v3/pulls/
# TODO: Scrape student info from: https://api.github.com/users/<student-account>/events/public?
# TODO: Rewrite git history?
# TODO: Automate removal of forked repo from your account.
# TODO: Move away from brew as a dependency manager.
# TODO: Support command line args for student repo url or a file of urls.
# TODO: Support cli option to write a custom pull request message.
# TODO: Don't hardcode BlissChapman :p

# Install dependencies if necessary.
brew install hub

# Clone student repository.
git clone https://github.com/cs225uiuc/test tmp
cd tmp

# Fix their code for them :-)
git checkout -b "feature/fixed-it"
rm -R */
git add .
git commit -m "fixed it for you <3"

# Fork the student repository.
hub fork
hub remote add BlissChapman
git push BlissChapman feature/fixed-it

# Open a pull request into the student repository.
hub pull-request

# TODO: Auto-fill standard pull request message unless flag is set.

# Cleanup
cd ..
rm -rf tmp/